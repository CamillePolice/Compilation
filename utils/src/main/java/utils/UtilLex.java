package utils;
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import utils.Ecriture;

public class UtilLex {
    public static String nomSource;
    public static int numId = 0;
    public static int valNb = 0;
    private static String tampon = " L";
    private static String exTampon = " L";
    private static int numLigne = 0;
    public static int numColonne = 0;
    private static int fenetre = 1;
    private static int exFenetre;
    private static final int MAXID = 2000;
    private static int nbId = 0;
    private static String[] identificateurs = new String[2000];

    public UtilLex() {
    }

    private static void arret() {
        System.out.println("erreur, arret de la compilation");
        System.exit(0);
    }

    public static void messErr(String message) {
        System.out.println("erreur, ligne numero : " + numLigne);
        System.out.println("colonne numero  : " + numColonne);
        System.out.println(message);
        arret();
    }

    private static int chercherId(String id) {
        int indice;
        for(indice = 0; indice < nbId && !id.equals(identificateurs[indice]); ++indice) {
            ;
        }

        if (indice == nbId) {
            if (nbId == 2000) {
                messErr("Débordement de la table des identificateurs");
            }

            identificateurs[nbId] = id;
            ++nbId;
        }

        return indice;
    }

    public static String repId(int i) {
        if (i >= nbId || i < 0) {
            messErr("repid sur num ident erroné");
        }

        return identificateurs[i];
    }

    public static void traiterId(String id, int numeroLigne, int numeroColonne) {
        numLigne = numeroLigne;
        numColonne = numeroColonne;
        numId = chercherId(id.toLowerCase());
    }

    private static void afftabIdentificateurs() {
        System.out.println("       numero           libelle     ");
        System.out.println("      |--------------|--------------");

        for(int i = 0; i < nbId; ++i) {
            Ecriture.ecrireInt(i, 6);
            if (identificateurs[i] == null) {
                System.out.println(" référence NULL");
            } else {
                System.out.println(" " + identificateurs[i]);
            }
        }

        System.out.println();
    }
}
