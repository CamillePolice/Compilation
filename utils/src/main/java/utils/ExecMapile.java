package utils;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import java.io.InputStream;

public class ExecMapile {
    private static final int MAXPILE = 1000;
    private static final int MAXOBJ = 6000;
    private static int[] pile = new int[1001];
    private static int[] po = new int[6001];
    private static int ip = -1;
    private static int bp = 0;
    private static int tailleProg;
    private static int ipo = 0;
    private static int ipoCour;
    private static int codeInst;
    private static int arg1;
    private static int arg2;
    private static final int[] numControle = new int[]{0, 1, 2, 3, 4, 5, 5, 6, 5, 5, 5, 5, 5, 5, 5, 5, 5, 7, 8, 9, 2, 2, 6, 14, 0, 3, 10, 10, 11, 12, 13};

    public ExecMapile() {
    }

    private static void erreur(String var0) {
        System.out.println(var0);
        System.exit(1);
    }

    private static void erreurInst(String var0) {
        System.out.println("erreur fatale sur l'instruction :");
        ecrireInst(ipoCour, codeInst, arg1, arg2);
        erreur(var0);
    }

    private static void ecrireInst(int var0, int var1, int var2, int var3) {
        Ecriture.ecrireInt(var0, 4);
        Ecriture.ecrireString("  " + Mnemo.inst[var1]);
        switch(Mnemo.nbp[var1]) {
            case 1:
                Ecriture.ecrireInt(var2, 7);
                break;
            case 2:
                Ecriture.ecrireInt(var2, 7);
                Ecriture.ecrireInt(var3, 4);
        }

        Ecriture.ecrireStringln("");
    }

    private static void testPile(int var0) {
        if (var0 < -1 || var0 > 1000) {
            erreurInst("indice " + var0 + " hors pile");
        }

    }

    private static void validAdPile(int var0) {
        if (var0 < 0 || var0 > ip) {
            erreurInst("pile[" + var0 + "] illicite (ip vaut " + ip + ")");
        }

    }

    private static void valid2AdPile(int var0) {
        if (var0 < -1 || var0 > ip) {
            erreurInst("pile[" + var0 + "] illicite (ip vaut " + ip + ")");
        }

    }

    private static void ipPlusGrandQue(int var0) {
        if (ip < var0) {
            erreurInst("ip(" + ip + ") devrait être au moins égal à " + var0);
        }

    }

    private static void validDiviseur() {
        if (pile[ip] == 0) {
            erreurInst("division par zéro");
        }

    }

    private static void validObj(int var0) {
        if (var0 <= 0 || var0 > tailleProg) {
            erreurInst("adresse programme objet " + var0 + " incorrecte (taillePo=" + tailleProg + ")");
        }

    }

    private static void validBool() {
        if (pile[ip] != 0 && pile[ip] != 1) {
            erreurInst("le sommet de pile n'est pas booléen (" + pile[ip] + ")");
        }

    }

    private static void validAdIndir() {
        validAdPile(bp + arg1);
        switch(arg2) {
            case 0:
                break;
            case 1:
                validAdPile(pile[bp + arg1]);
                break;
            default:
                erreurInst("bit d'indirection incorrect : " + arg2);
        }

    }

    private static void controleInst() {
        validObj(ipo - 1);
        switch(numControle[codeInst]) {
            case 1:
                testPile(ip + arg1);
                break;
            case 2:
                testPile(ip + 1);
                break;
            case 3:
                testPile(ip + 1);
                validAdPile(arg1);
                break;
            case 4:
                validAdPile(arg1);
                ipPlusGrandQue(1);
                break;
            case 5:
                ipPlusGrandQue(1);
                break;
            case 6:
                ipPlusGrandQue(0);
                break;
            case 7:
                ipPlusGrandQue(1);
                validDiviseur();
                break;
            case 8:
                ipPlusGrandQue(0);
                validObj(arg1);
                break;
            case 9:
                validObj(arg1);
                break;
            case 10:
                testPile(ip + 1);
                validAdIndir();
                break;
            case 11:
                ipPlusGrandQue(1);
                validAdIndir();
                break;
            case 12:
                testPile(ip + 2);
                validObj(arg1);
                valid2AdPile(ip - arg2);
                break;
            case 13:
                validAdPile(bp + arg1 + 1);
                validAdPile(bp + arg1);
                validObj(pile[bp + arg1 + 1]);
                testPile(bp - 1);
                break;
            case 14:
                ipPlusGrandQue(0);
                validBool();
        }

    }

    private static void reserver(int var0) {
        for(int var1 = 1; var1 <= var0; ++var1) {
            pile[ip + var1] = 0;
        }

        ip += var0;
    }

    private static void empiler(int var0) {
        ++ip;
        pile[ip] = var0;
    }

    private static void contenuG(int var0) {
        ++ip;
        pile[ip] = pile[var0];
    }

    private static void affecterG(int var0) {
        pile[var0] = pile[ip];
        --ip;
    }

    private static void ou() {
        --ip;
        if (pile[ip] == 0) {
            pile[ip] = pile[ip + 1];
        }

    }

    private static void et() {
        --ip;
        if (pile[ip] == 1) {
            pile[ip] = pile[ip + 1];
        }

    }

    private static void non() {
        pile[ip] = 1 - pile[ip];
    }

    private static void inf() {
        --ip;
        pile[ip] = pile[ip] < pile[ip + 1] ? 1 : 0;
    }

    private static void infEg() {
        --ip;
        pile[ip] = pile[ip] <= pile[ip + 1] ? 1 : 0;
    }

    private static void sup() {
        --ip;
        pile[ip] = pile[ip] > pile[ip + 1] ? 1 : 0;
    }

    private static void supEg() {
        --ip;
        pile[ip] = pile[ip] >= pile[ip + 1] ? 1 : 0;
    }

    private static void eg() {
        --ip;
        pile[ip] = pile[ip] == pile[ip + 1] ? 1 : 0;
    }

    private static void diff() {
        --ip;
        pile[ip] = pile[ip] != pile[ip + 1] ? 1 : 0;
    }

    private static void add() {
        --ip;
        pile[ip] += pile[ip + 1];
    }

    private static void sous() {
        --ip;
        pile[ip] -= pile[ip + 1];
    }

    private static void mul() {
        --ip;
        pile[ip] *= pile[ip + 1];
    }

    private static void div() {
        --ip;
        pile[ip] /= pile[ip + 1];
    }

    private static void bSiFaux(int var0) {
        if (pile[ip] == 0) {
            ipo = var0;
        }

        --ip;
    }

    private static void bIncond(int var0) {
        ipo = var0;
    }

    private static void lireEnt() {
        boolean var0;
        int var1;
        do {
            try {
                System.out.print("Donner un entier : ");
                var1 = Lecture.lireInt();
                var0 = true;
            } catch (NumberFormatException var3) {
                var1 = 0;
                var0 = false;
            }
        } while(!var0);

        ++ip;
        pile[ip] = var1;
    }

    private static void lireBool() {
        char var0;
        do {
            System.out.print("Donner un booléen (V ou F) : ");
            String var1 = Lecture.lireString();
            if (var1.equals("")) {
                var0 = ' ';
            } else {
                var0 = var1.charAt(0);
            }
        } while("vVfF".indexOf(var0) == -1);

        ++ip;
        if (var0 != 'f' && var0 != 'F') {
            pile[ip] = 1;
        } else {
            pile[ip] = 0;
        }

    }

    private static void ecrEnt() {
        System.out.println("Expression entière de valeur : " + pile[ip]);
        --ip;
    }

    private static void ecrBool() {
        System.out.print("Expression booléenne de valeur : ");
        System.out.println(pile[ip] == 0 ? "F" : "V");
        --ip;
    }

    private static void empilerAdG(int var0) {
        ++ip;
        pile[ip] = var0;
    }

    private static int adFinale(int var0, int var1) {
        return var1 == 0 ? bp + var0 : pile[bp + var0];
    }

    private static void empilerAdL(int var0, int var1) {
        ++ip;
        pile[ip] = adFinale(var0, var1);
    }

    private static void contenuL(int var0, int var1) {
        ++ip;
        pile[ip] = pile[adFinale(var0, var1)];
    }

    private static void affecterL(int var0, int var1) {
        pile[adFinale(var0, var1)] = pile[ip];
        --ip;
    }

    private static void appel(int var0, int var1) {
        ++ip;
        pile[ip] = bp;
        bp = ip - var1;
        ++ip;
        pile[ip] = ipoCour + 3;
        ipo = var0;
    }

    private static void retour(int var0) {
        ipo = pile[bp + var0 + 1];
        int var1 = pile[bp + var0];
        ip = bp - 1;
        bp = var1;
    }

    public static void activer() {
        System.out.println("nom du fichier exécutable, SANS suffixe");
        System.out.print("(terminer par * pour avoir une trace d'exécution) : ");
        String var0 = Lecture.lireString();
        boolean var1;
        if (var0.length() != 0 && var0.charAt(var0.length() - 1) == '*') {
            var1 = true;
            var0 = var0.substring(0, var0.length() - 1);
        } else {
            var1 = false;
        }

        InputStream var2 = Lecture.ouvrir(var0 + ".map");
        if (var2 == null) {
            var2 = Lecture.ouvrir(var0 + ".obj");
            if (var2 == null) {
                System.out.println("il n'y a pas de fichier " + var0 + " .map ou .obj accessible");
                System.exit(1);
            }
        }

        for(; !Lecture.finFichier(var2); po[ipo] = Lecture.lireIntln(var2)) {
            ++ipo;
            if (ipo > 6000) {
                erreur("Programme exécutable trop gros");
            }
        }

        Lecture.fermer(var2);
        tailleProg = ipo;
        ipo = 1;

        for(codeInst = po[ipo]; codeInst != 24; codeInst = po[ipo]) {
            if (codeInst < 1 || codeInst >= Mnemo.inst.length) {
                System.out.println("en " + ipo + " : " + codeInst + " n'est pas un code MAPILE");
                System.exit(1);
            }

            if (ipo + Mnemo.nbp[codeInst] > 6000) {
                System.out.println("erreur : ipo = " + ipo + ", code instruction = " + codeInst + " et un argument est en dehors de po");
                System.exit(1);
            }

            ipoCour = ipo;
            if (Mnemo.nbp[codeInst] != 0) {
                ++ipo;
                arg1 = po[ipo];
            }

            if (Mnemo.nbp[codeInst] == 2) {
                ++ipo;
                arg2 = po[ipo];
            }

            if (var1) {
                ecrireInst(ipoCour, codeInst, arg1, arg2);
            }

            ++ipo;
            controleInst();
            switch(codeInst) {
                case 1:
                    reserver(arg1);
                    break;
                case 2:
                    empiler(arg1);
                    break;
                case 3:
                    contenuG(arg1);
                    break;
                case 4:
                    affecterG(arg1);
                    break;
                case 5:
                    ou();
                    break;
                case 6:
                    et();
                    break;
                case 7:
                    non();
                    break;
                case 8:
                    inf();
                    break;
                case 9:
                    infEg();
                    break;
                case 10:
                    sup();
                    break;
                case 11:
                    supEg();
                    break;
                case 12:
                    eg();
                    break;
                case 13:
                    diff();
                    break;
                case 14:
                    add();
                    break;
                case 15:
                    sous();
                    break;
                case 16:
                    mul();
                    break;
                case 17:
                    div();
                    break;
                case 18:
                    bSiFaux(arg1);
                    break;
                case 19:
                    bIncond(arg1);
                    break;
                case 20:
                    lireEnt();
                    break;
                case 21:
                    lireBool();
                    break;
                case 22:
                    ecrEnt();
                    break;
                case 23:
                    ecrBool();
                case 24:
                default:
                    break;
                case 25:
                    empilerAdG(arg1);
                    break;
                case 26:
                    empilerAdL(arg1, arg2);
                    break;
                case 27:
                    contenuL(arg1, arg2);
                    break;
                case 28:
                    affecterL(arg1, arg2);
                    break;
                case 29:
                    appel(arg1, arg2);
                    break;
                case 30:
                    retour(arg1);
            }
        }

        System.out.println();
        System.out.println("Exécution terminée");
    }
}

