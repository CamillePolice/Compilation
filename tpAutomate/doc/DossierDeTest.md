# Dosssier de test

## Test effectué

- Test de l'analyseur lexical en suivant pas à pas ce que l'analyseur comprend du texte.
- Création d'un nouveau fichier de test:  
```	
    CAMILLE 200   		
   		BOURGOGNE    superu 60 ,   			
   		BEAUJOLAIS   leclerc  150   elephant 30 ;
   		   
   	CHARLES  110   		
   		BEAUJOLAIS    elephant  50;	
   		
   	QUENTIN   130   	
   		grandfrais  110,   		
   		BEAUJOLAIS    potin   60 ;
   	
   	CHARLES    		
   		carrefour  30    leclerc 70;
   			
   	
   	QUENTIN 90    	 
   		maisondumonde  50;
   		
   	CAMILLE   230    	
   		superu    30     maisondumonde 70;
   	
   	POTIN  150    	
   		potin   60    grandfrais	50,   
   		BOURGOGNE   grandfrais   30;
   		
   	CHARLES    		
   		BEAUJOLAIS ; carrefour 60,   		
   		potin  40;
   		
   	ROBERT    	
   		; superu 50  grandfrais 70,   		
   		BEAUJOLAIS elephant 60;
   		
   	50    CAMILLE   	
   		BOURGOGNE    marie  20,	   		
   		carrefour    40;
   
   	QUENTIN    140   	
   		BOURGOGNE  potin  50  leclerc 70;   		
   		grandfrais   80;	
   		
   /
   ```
   Nous avons regarder pas à pas chaque étape de l'automate afin de vérifier son bon fonctionnement.

## Cas d'erreur non résolu

Dans le cas suivant:
```text
DURAND 120

  BEAUJOLAIS ; vieillesgaleries 50  prixmultiples 70 ,

  prixmultiples 110,

  BOURGOGNE      potin 50 vieillesgaleries 40 elephant 10;
```

L'interpreteur pense que le texte est correcte car il suit correctement l'automate.