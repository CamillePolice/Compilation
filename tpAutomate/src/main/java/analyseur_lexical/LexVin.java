package analyseur_lexical;

import utils.Lecture;

import java.io.InputStream;
import java.util.Arrays;

/**
 * Analyseur lexical pour TP livraison vins
 *
 * @author Charles Delorme, Camille Police, Quentin Loustau
 */
public class LexVin extends Lex {

    public static final String[] images = {"BEAUJ", "BOURG", "IDENT", "NBENT", "  ,  ",
            "  ;  ", "  /  ", "AUTRE"};
    /**
     * codage des items lexicaux
     */
    protected final int
            BEAUJOLAIS = 0, BOURGOGNE = 1, IDENT = 2, NBENTIER = 3,
            VIRGULE = 4, PTVIRG = 5, BARRE = 6, AUTRES = 7;
    /**
     * nombre de mots r�serv�s dans Lex.tabId
     */
    private final int NBRES = 2;
    /**
     * indice de remplissage de Lex.tabid
     */
    private int itab;

    /**
     * attributs lexicaux
     */
    private int valNb, numId;

    /**
     * caractere courant
     */
    private char carlu;

    /**
     * constructeur classe LexVin
     */
    public LexVin(InputStream flot) {
        /** initialisation du flot par la classe abstraite */
        super(flot);
        /** prelecture du premier caractere de la donnee */
        lireCarlu();
        /** initialisation tabId par mots reserves */
        tabId[0] = "BEAUJOLAIS";
        tabId[1] = "BOURGOGNE";
        itab = 1;
    }

    /**
     * Main pour tester l'analyseur lexical seul (sans analyse syntaxique)
     */
    public static void main(String args[]) {

        String nomfich;
        nomfich = Lecture.lireString("nom du fichier d'entree : ");
        InputStream flot = Lecture.ouvrir(nomfich);
        if (flot == null) {
            System.exit(0);
        }

        LexVin testVin = new LexVin(flot);
        testVin.testeur_lexical();

        Lecture.fermer(flot);
        Lecture.attenteSurLecture("fin d'analyse");
        System.exit(0);

    }

    public char getCarlu() {
        return this.carlu;
    }

    /**
     * procedure de lecture du caractere courant
     */
    private void lireCarlu() {
        carlu = Lecture.lireChar(flot);
        this.notifyObservers(carlu);
        if ((carlu == '\r') || (carlu == '\n') || (carlu == '\t'))
            carlu = ' ';
        if (Character.isWhitespace(carlu))
            carlu = ' ';
        else
            carlu = Character.toUpperCase(carlu);
    }

    /**
     * lecture item lexical NBENTIER et maj de l'attribut lexical valNb
     *
     * @return code NBENTIER
     */
    private int lireEnt() {
        String s = "";
        do {
            s += carlu;
            lireCarlu();
        } while ((carlu >= '0') && (carlu <= '9'));
        valNb = Integer.parseInt(s);
        return NBENTIER;
    }

    public int lireWord() {
        String s = "";
        do {
            s += carlu;
            lireCarlu();
        } while ((carlu >= 'A') && (carlu <= 'Z'));
        if (tabId[0].equals(s) || tabId[1].equals(s)) {
            return Arrays.asList(tabId).indexOf(s);
        } else if (Arrays.asList(tabId).contains(s)) {
            numId = Arrays.asList(tabId).indexOf(s);
            return IDENT;
        } else {
            itab++;
            tabId[itab] = s;
            numId = Arrays.asList(tabId).indexOf(s);
            return IDENT;
        }
    } /** liresymb */

    /**
     * determination de l'item lexical
     * definition de la methode abstraite lireSymb de Lex
     *
     * @return code entier de l'item lexical reconnu
     */
    public int lireSymb() {

        while (carlu == ' ')
            lireCarlu();

        if (carlu >= '0' && carlu <= '9')
            return lireEnt();

        if (carlu >= 'A' && carlu <= 'Z')
            return lireWord();

        /** detection autres items lexicaux */
        switch (carlu) {
            case ',':
                lireCarlu();
                return VIRGULE;
            case ';':
                lireCarlu();
                return PTVIRG;
            case '/':
                return BARRE;
            default:
                System.out.println("LexExp : caract�re incorrect : " + carlu);
                lireCarlu();
                return AUTRES;
        }
    }

    /**
     * fonction donnant la chaine associee a un ident de tabId
     * definition de la methode abstraite repId de Lex
     *
     * @param nId indice de l'ident dans tabId
     * @return chaine associee a l'ident
     */
    public String repId(int nId) {
        return tabId[nId];
    }

    /**
     * methodes d'acces aux attributs lexicaux
     */
    public int getValNb() {
        return this.valNb;
    }

    public int getNumId() {
        return this.numId;
    }

    /**
     * utilitaire de test de l'analyseur lexical seul (sans analyse syntaxique)
     */
    private void testeur_lexical() {
        /** Unite lexicale courante */
        int token;
        /** definition du caractere de fin de chaine
         * utile uniquement pour text autonome du lexical*/
        int finDeChaine = BARRE;
        do {
            token = lireSymb();
            if (token == NBENTIER)
                Lecture.attenteSurLecture("token : " + images[token] + " attribut valNB = " + valNb);
            else if (token == IDENT)
                Lecture.attenteSurLecture("token : " + images[token] + " attribut numId = " + numId);
            else
                Lecture.attenteSurLecture("token : " + images[token]);
        } while (token != finDeChaine);
    }

} /**
 * class Lexvin
 */
