package analyseur_syntaxique;

import data.Chauffeur;
import utils.Ecriture;
import utils.Lecture;
import utils.SmallSet;

import java.io.InputStream;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * gestion des actions associees a la reconnaissance des fiches de livraison de vin
 *
 * @author Charles Delorme, Camille Police, Quentin Lousteau
 */
public class ActVin extends AutoVin {

    /**
     * types d'erreurs detectees
     */
    private static final int FATALE = 0, NONFATALE = 1;
    /**
     * taille d'une colonne pour affichage ecran
     */
    private static final int MAXLGID = 20;
    /**
     * nombre maximum de chauffeurs
     */
    private static final int MAXCHAUF = 10;
    /**
     * table des actions
     */
    private final int[][] action =
            {/* �tat     BJ BG IDENT NBENT ,  ;    /  AUTRES  */
    /* 0 */      {10, 10, 1, 10, 10, 10, 9, -1},
    /* 1 */      {3, 4, 5, 2, 10, 10, 10 - 1},
    /* 2 */      {3, 4, 5, 10, 10, 10, 10, -1},
    /* 3 */      {10, 10, 6, 10, 10, 10, 10, -1},
    /* 4 */      {10, 10, 10, 7, 10, 10, 10, -1},
    /* 5 */      {10, 10, 6, 10, -1, 8, 10, -1},
    /* 6 */      {-1, -1, -1, -1, -1, -1, -1, -1},
            };
    /**
     * tableau des chauffeurs et resume des livraison de chacun
     */

    private Map<Integer, Chauffeur> mapChauf = new HashMap<>(MAXCHAUF);
    /**
     * indice courant du nombre de chauffeurs dans le tableau tabChauf
     */
    private int ichauf;
    //Chauffeur actuel
    private Chauffeur chauffeur;
    /**
     * variables necessaires aux traitements effectues dans les actions
     */
    private String vin;
    /*!!! DELARATIONS A COMPLETER !!!*/
    private int volumeCiterne;

    /**
     * constructeur classe ActVin
     */
    public ActVin(InputStream flot) {
        super(flot);
    }

    /**
     * utilitaire d'affichage a l'ecran
     *
     * @param ch est une chaine de longueur quelconque
     * @return chaine ch cadree a gauche sur MAXLGID caracteres
     */
    private String chaineCadrageGauche(String ch) {
        int lgch = Math.min(MAXLGID, ch.length());
        String chres = ch.substring(0, lgch);
        for (int k = lgch; k < MAXLGID; k++)
            chres = chres + " ";
        return chres;
    }

    private void afficherMeilleurChauf() {
        mapChauf.values().stream().max(Comparator.comparingInt(c -> c.magdif.size()))
                .ifPresent(chauffeur -> Ecriture.ecrireStringln("Meilleur chauffeur: " + chauffeur));
    }

    /**
     * affichage de tout le tableau de chauffeurs a l'ecran
     */
    private void afficherchauf() {
        Ecriture.ecrireStringln("");
        Ecriture.ecrireStringln("");
        Ecriture.ecrireStringln("CHAUFFEUR                   BJ        BG       ORD     NBMAG\n"
                + "---------                   --        --       ---     -----");
        mapChauf.values().forEach(chauf -> {
            Ecriture.ecrireString(chaineCadrageGauche(Integer.toString(chauf.numchauf)));
            Ecriture.ecrireInt(chauf.bj, 10);
            Ecriture.ecrireInt(chauf.bg, 10);
            Ecriture.ecrireInt(chauf.ordin, 10);
            Ecriture.ecrireInt(chauf.magdif.size(), 10);
            Ecriture.ecrireStringln("");
        });
    }

    /**
     * gestion des erreurs
     *
     * @param te      type de l'erreur
     * @param messErr message associe a l'erreur
     */
    private void erreur(int te, String messErr) {
        Lecture.attenteSurLecture(messErr);
        switch (te) {
            case FATALE:
                errFatale = true;
                break;
            case NONFATALE:
                etatCourant = etatErreur;
                break;
            default:
                Lecture.attenteSurLecture("parametre incorrect pour erreur");
        }
    }

    /**
     * initialisations a effectuer avant les actions
     */
    private void initialisations() {
        ichauf = volumeCiterne = -1;
    }

    /**
     * acces a un attribut lexical
     * cast pour preciser que lex est de type LexVin
     *
     * @return valNb associe a l'unite lexicale nbentier
     */
    private int valNb() {
        return ((LexVin) lex).getValNb();
    }

    /**
     * acces a un attribut lexical
     * cast pour preciser que lex est de type LexVin
     *
     * @return numId associe a l'unite lexicale ident
     */
    private int numId() {
        return ((LexVin) lex).getNumId();
    }


    /**
     * Permet d'obtenir un chauffeur en fonction de son numéro, et en créer un
     * si il n'existe pas
     *
     * @param numeroChauffeur numéro du chauffeur
     * @return Chauffeur correspondant au numéro donné en paramètre ou un nouveau chauffeur
     */
    private Chauffeur getChauffeur(int numeroChauffeur) {
        mapChauf.putIfAbsent(numeroChauffeur, new Chauffeur(numeroChauffeur, 0, 0, 0, new SmallSet()));
        return mapChauf.get(numeroChauffeur);
    }

    /**
     * execution d'une action
     *
     * @param numact numero de l'action a executer
     */
    private void executer(int numact) {
        //System.out.println("Chauffeur="+chauffeur);
        //System.out.println("valNb="+valNb());
        switch (numact) {
            case -1:
                break;
            // detection du numéro du chauffeur
            case 1:
                ichauf = getChauffeur(valNb()).numchauf;
                break;
            case 2:
                if (valNb() >= 100 && valNb() <= 200) {
                    volumeCiterne = valNb();
                } else {
                    volumeCiterne = 100;
                    erreur(1, "Capacitée de la citerne forcée à 100 car la " +
                            "valeur actuelle est incorrecte (" + valNb() + ")");
                }
                break;
            // Detection du vin
            case 3:
                vin = "BJ";
                break;
            case 4:
                vin = "BG";
                break;
            case 5:
                vin = "ORD";
                chauffeur = getChauffeur(ichauf);
                chauffeur.magdif.add(valNb());
                break;
            case 6:
                // Ajout du magasin
                chauffeur = getChauffeur(ichauf);
                chauffeur.magdif.add(valNb());
                break;
            case 7:
                // Ajout des quantité livré par le chauffeur
                chauffeur = getChauffeur(ichauf);
                switch (vin) {
                    case "BJ":
                        chauffeur.bj += valNb();
                        break;
                    case "BG":
                        chauffeur.bg += valNb();
                        break;
                    case "ORD":
                        chauffeur.ordin += valNb();
                        break;
                    default:
                        erreur(1, "Vin inconnue, quantitée non ajoutée");
                }
                break;
            case 8:
                afficherchauf();
                break;
            case 9:
                afficherMeilleurChauf();
                break;
            case 10:
                erreur(1, "Erreur, syntaxe non permise");
                break;
            default:
                Lecture.attenteSurLecture("action " + numact + " non prevue");
        }
    }

    /**
     * definition methode abstraite faireAction de Automate
     */
    public void faireAction(int etat, int unite) {
        executer(action[etat][unite]);
    }

    /**
     * definition methode abstraite initAction de Automate
     */
    public void initAction() {
        // action 0 a effectuer a l'init
        initialisations();
    }


    /**
     * definition methode abstraite getAction de Automate
     */
    public int getAction(int etat, int unite) {
        return action[etat][unite];
    }
}
