package analyseur_syntaxique;

import utils.ObserverAutomate;
import utils.ObserverLexique;

import java.io.InputStream;

/**
 * automate de reconnaissance des fiches de livraison de vin
 *
 * @author ?? MERCI DE PRECISER LE NOM DU TRINOME ??
 */
public class AutoVin extends Automate {

    /**
     * table des transitions
     */
    private final int[][] transit =
            {   /* Etat        BJ   BG   IDENT  NBENT  ,    ;    /  AUTRES  */
         /* 0 */      {6, 6, 1, 6, 6, 0, 7, 6},
	 	/* 1 */      {3, 3, 4, 2, 6, 0, 6, 6},
		/* 2 */      {3, 3, 4, 6, 6, 0, 6, 6},
		/* 3 */      {6, 6, 4, 6, 6, 0, 6, 6},
		/* 4 */      {6, 6, 6, 5, 6, 0, 6, 6},
		/* 5 */      {6, 6, 4, 6, 2, 0, 6, 6},
		/* 6 */      {6, 6, 6, 6, 6, 0, 6, 6},
            };

    /**
     * constructeur classe AutoVin
     */
    public AutoVin(InputStream flot) {
        /** on utilise ici un analyseur lexical de type LexVin */
        lex = new LexVin(flot);
        this.etatInitial = 0;
        this.etatFinal = transit.length;
        this.etatErreur = transit.length - 1;
    }

    /**
     * utilitaire de suivi des modifications pour affichage
     */
    public void newObserver(ObserverAutomate oAuto, ObserverLexique oLex) {
        this.newObserver(oAuto);
        this.lex.newObserver(oLex);
        lex.notifyObservers(((LexVin) lex).getCarlu());
    }

    /**
     * definition de la methode abstraite getTransition de Automate
     */
    int getTransition(int etat, int unite) {
        return this.transit[etat][unite];
    }

    /**
     * ici la methode abstraite faireAction de Automate n'est pas encore definie
     */
    void faireAction(int etat, int unite) {
    }

    ;

    /**
     * ici la methode abstraite actionInit de Automate n'est pas encore definie
     */
    void initAction() {
    }

    ;

    /**
     * ici la methode abstraite getAction de Automate n'est pas encore definie
     */
    int getAction(int etat, int unite) {
        return 0;
    }

    ;

}/**
 * AutoVin
 */
