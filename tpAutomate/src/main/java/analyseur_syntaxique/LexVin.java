package analyseur_syntaxique;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import utils.Lecture;

import java.io.InputStream;

public class LexVin extends Lex {
    public static final String[] images = new String[]{"BEAUJ", "BOURG", "IDENT", "NBENT", "  ,  ", "  ;  ", "  /  ", "AUTRE"};
    private static final int[][] transit = new int[][]{{3, 1, 2, 2, 5, 0, 4}, {5, 1, 5, 5, 5, 5, 5}, {5, 5, 5, 5, 5, 5, 5}, {3, 5, 5, 5, 5, 5, 5}, {5, 5, 5, 5, 5, 5, 5}, {5, 5, 5, 5, 5, 5, 5}};
    private static final int FINAL = 5;
    private static final int[][] action = new int[][]{{4, 1, 7, 8, 9, -1, 10}, {-1, 2, 3, 3, 3, 3, 3}, {-1, -1, -1, -1, -1, -1, -1}, {5, 6, 6, 6, 6, 6, 6}, {-1, -1, -1, -1, -1, -1, -1}, {-1, -1, -1, -1, -1, -1, -1}};
    private static String courantIdent = "";
    private static int courantNbentier;
    private static int uniteLexicale;
    protected final int BEAUJOLAIS = 0;
    protected final int BOURGOGNE = 1;
    protected final int IDENT = 2;
    protected final int NBENTIER = 3;
    protected final int VIRGULE = 4;
    protected final int PTVIRG = 5;
    protected final int BARRE = 6;
    protected final int AUTRES = 7;
    protected final int cl_lettre = 0;
    protected final int cl_chiffre = 1;
    protected final int cl_virgule = 2;
    protected final int cl_pointvirgule = 3;
    protected final int cl_barre = 4;
    protected final int cl_espace = 5;
    protected final int cl_autre = 6;
    private final int NBRES = 3;
    int finDeChaine = 6;
    private int itab;
    private int valNb;
    private int numId;
    private char carlu;

    public LexVin(InputStream flot) {
        super(flot);
        System.out.println("Corrig� Lexvin fourni par les enseignants");
        System.out.println("--------------");
        this.lireCarlu();
        tabId[0] = "BEAUJOLAIS";
        tabId[1] = "BOURGOGNE";
        tabId[2] = "PASTRICHER";
        this.itab = 2;
    }

    public static void main(String[] args) {
        String nomfich = Lecture.lireString("nom du fichier d\'entree : ");
        InputStream flot = Lecture.ouvrir(nomfich);
        if (flot == null) {
            System.exit(0);
        }

        LexVin testVin = new LexVin(flot);
        testVin.testeur_lexical();
        Lecture.fermer(flot);
        Lecture.attenteSurLecture("fin d\'analyse");
        System.exit(0);
    }

    public char getCarlu() {
        return this.carlu;
    }

    private void lireCarlu() {
        this.carlu = Lecture.lireChar(this.flot);
        this.notifyObservers(this.carlu);
        if (this.carlu == 13 || this.carlu == 10 || this.carlu == 9) {
            this.carlu = 32;
        }

        if (Character.isWhitespace(this.carlu)) {
            this.carlu = 32;
        } else {
            this.carlu = Character.toUpperCase(this.carlu);
        }

    }

    protected int classe(char c) {
        return c >= 65 && c <= 90 ? 0 : (c >= 48 && c <= 57 ? 1 : (c == 44 ? 2 : (c == 59 ? 3 : (c == 47 ? 4 : (c != 32 && c != 9 && c != 10 && c != 13 ? 6 : 5)))));
    }

    private void l_executer(int numact) {
        switch (numact) {
            case -1:
            case 0:
                break;
            case 1:
                courantNbentier = this.carlu - 48;
                break;
            case 2:
                courantNbentier = courantNbentier * 10 + (this.carlu - 48);
                break;
            case 3:
                uniteLexicale = 3;
                this.valNb = courantNbentier;
                break;
            case 4:
                courantIdent = "" + this.carlu;
                break;
            case 5:
                courantIdent = courantIdent + this.carlu;
                break;
            case 6:
                int indexTab;
                for (indexTab = 0; indexTab <= this.itab && !courantIdent.equals(tabId[indexTab]); ++indexTab) {
                    ;
                }

                if (indexTab < 3) {
                    uniteLexicale = indexTab;
                } else if (indexTab <= this.itab) {
                    this.numId = indexTab;
                    uniteLexicale = 2;
                } else if (this.itab < 199) {
                    ++this.itab;
                    tabId[this.itab] = new String(courantIdent);
                    this.numId = this.itab;
                    uniteLexicale = 2;
                } else {
                    Lecture.attenteSurLecture("Lexvin : debordement de la table des identificateurs");
                    System.exit(0);
                }
                break;
            case 7:
                uniteLexicale = 4;
                break;
            case 8:
                uniteLexicale = 5;
                break;
            case 9:
                uniteLexicale = 6;
                break;
            case 10:
                uniteLexicale = 7;
                break;
            case 11:
                Lecture.attenteSurLecture("Erreur au niveau lexical, caractere " + this.carlu + " non prevu pour ce langage");
                uniteLexicale = 7;
                break;
            default:
                Lecture.attenteSurLecture("Erreur appel d\'une action lexicale " + numact + " non definie");
        }

    }

    public int lireSymb() {
        this.l_executer(0);
        byte etat = 0;
        this.l_executer(action[etat][this.classe(this.carlu)]);

        for (int etat1 = transit[etat][this.classe(this.carlu)]; etat1 != 5; etat1 = transit[etat1][this.classe(this.carlu)]) {
            this.lireCarlu();
            this.l_executer(action[etat1][this.classe(this.carlu)]);
        }

        return uniteLexicale;
    }

    public String repId(int nId) {
        if (nId >= 3 && nId <= this.itab) {
            return new String(tabId[nId]);
        } else {
            Lecture.attenteSurLecture("Lexvin.repId : numero d\'ident incorrect");
            System.exit(0);
            return "";
        }
    }

    public int getValNb() {
        return this.valNb;
    }

    public int getNumId() {
        return this.numId;
    }

    private void testeur_lexical() {
        byte finDeChaine = 6;

        int token;
        do {
            token = this.lireSymb();
            if (token == 3) {
                Lecture.attenteSurLecture("token : " + images[token] + " attribut valNB = " + this.valNb);
            } else if (token == 2) {
                Lecture.attenteSurLecture("token : " + images[token] + " attribut numId = " + this.numId);
            } else {
                Lecture.attenteSurLecture("token : " + images[token]);
            }
        } while (token != finDeChaine);

    }
}
