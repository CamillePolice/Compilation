package analyseur_syntaxique;

import utils.ObserverLexique;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * La classe Lex modelise un analyseur lexical abstrait
 *
 * @author Masson, Grazon
 */
abstract class Lex {

    /**
     * definition de la table des identificateurs
     */
    protected static final int MAXID = 200;
    protected static String[] tabId = new String[MAXID];
    /**
     * definition du flot d'entree a analyser
     */
    protected InputStream flot;
    /**
     * gestion de l'affichage sur la fenetre de trace de l'execution
     */
    private ArrayList<ObserverLexique> lesObserveurs = new ArrayList<ObserverLexique>();

    /**
     * constructeur de classe Lex
     */
    public Lex(InputStream f) {
        this.flot = f;
    }

    public void newObserver(ObserverLexique obs) {
        this.lesObserveurs.add(obs);
    }

    /**
     * fin gestion de l'affichage sur la fenetre de trace de l'execution
     */

    public void notifyObservers(char c) {
        for (ObserverLexique o : this.lesObserveurs) {
            o.nouveauChar(c);
        }
    }

    /**
     * methode d'acces a un item lexical
     */
    abstract int lireSymb();

    /**
     * methode d'acces a la chaine correspondant a un ident d'indice nId (dans tabId)
     **/
    abstract String repId(int nId);

} /**
 * class Lex
 */
