package utils;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

public class SmallSet {
    private static boolean firstTime = true;
    private boolean[] tab = new boolean[256];

    public SmallSet() {
        this.printOwner();

        for (int i = 0; i <= 255; ++i) {
            this.tab[i] = false;
        }

    }

    public SmallSet(boolean[] t) {
        this.printOwner();

        for (int i = 0; i <= 255; ++i) {
            this.tab[i] = t[i];
        }

    }

    public int size() {
        int nb = 0;

        for (int i = 0; i <= 255; ++i) {
            if (this.tab[i]) {
                ++nb;
            }
        }

        return nb;
    }

    public boolean contains(int x) {
        return x >= 0 && x <= 255 ? this.tab[x] : false;
    }

    public boolean isEmpty() {
        return this.size() == 0;
    }

    public void add(int x) {
        if (x >= 0 && x <= 255) {
            this.tab[x] = true;
        }

    }

    public void remove(int x) {
        if (x >= 0 && x <= 255) {
            this.tab[x] = false;
        }

    }

    public void addInterval(int deb, int fin) {
        int vraiDebut = deb >= 0 ? deb : 0;
        int vraieFin = fin <= 255 ? fin : 255;

        for (int i = vraiDebut; i <= vraieFin; ++i) {
            this.tab[i] = true;
        }

    }

    public void removeInterval(int deb, int fin) {
        int vraiDebut = deb >= 0 ? deb : 0;
        int vraieFin = fin <= 255 ? fin : 255;

        for (int i = vraiDebut; i <= vraieFin; ++i) {
            this.tab[i] = false;
        }

    }

    public void union(SmallSet f) {
        if (this != f) {
            for (int i = 0; i <= 255; ++i) {
                this.tab[i] = this.tab[i] || f.tab[i];
            }
        }

    }

    public void intersection(SmallSet f) {
        if (this != f) {
            for (int i = 0; i <= 255; ++i) {
                this.tab[i] = this.tab[i] && f.tab[i];
            }
        }

    }

    public void difference(SmallSet f) {
        if (this == f) {
            this.clear();
        } else {
            for (int i = 0; i <= 255; ++i) {
                this.tab[i] = this.tab[i] && !f.tab[i];
            }
        }

    }

    public void symmetricDifference(SmallSet f) {
        if (this == f) {
            this.clear();
        } else {
            for (int i = 0; i <= 255; ++i) {
                this.tab[i] ^= f.tab[i];
            }
        }

    }

    public void complement() {
        for (int i = 0; i <= 255; ++i) {
            this.tab[i] = !this.tab[i];
        }

    }

    public void clear() {
        for (int i = 0; i <= 255; ++i) {
            this.tab[i] = false;
        }

    }

    public boolean isIncludedIn(SmallSet f) {
        int i = 0;
        if (this == f) {
            return true;
        } else {
            while (i < 256 && (!this.tab[i] || f.tab[i])) {
                ++i;
            }

            return i == 256;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (!(obj instanceof SmallSet)) {
            return false;
        } else {
            SmallSet f = (SmallSet) obj;

            int i;
            for (i = 0; i < 256 && this.tab[i] == f.tab[i]; ++i) {
                ;
            }

            return i == 256;
        }
    }

    public SmallSet clone() {
        return new SmallSet(this.tab);
    }

    public String toString() {
        String s = "éléments présents : ";

        for (int i = 0; i <= 255; ++i) {
            if (this.tab[i]) {
                s = s + i + " ";
            }
        }

        return s;
    }

    private void printOwner() {
        if (firstTime) {
            System.out.println("classe Ensemble - version enseignant");
            firstTime = false;
        }

    }
}
