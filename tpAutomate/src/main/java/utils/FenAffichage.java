package utils;

/**
 * Created by 18000831 on 18/01/2018.
 */

import analyseur_lexical.LexVin;

import javax.swing.*;
import java.awt.*;

public class FenAffichage extends JFrame implements ObserverAutomate, ObserverLexique {
    private static final long serialVersionUID = 1L;
    TextArea fenEntree;
    TextArea fenTrace;

    public FenAffichage() {
        this.setDefaultCloseOperation(3);
        this.setTitle("Affichage des traces de l\'automate");
        this.setLocation(430, 0);
        this.fenEntree = new TextArea("FICHIER D\'ENTREE\n", 9, 80);
        this.fenEntree.append("----------------\n");
        this.fenEntree.setFont(new Font("Courier", 0, 12));
        this.fenEntree.setEditable(false);
        this.add(this.fenEntree, "North");
        this.fenTrace = new TextArea("ETAT, UNITE, ETAT, ACTION\n", 10, 80);
        this.fenTrace.append("-------------------------\n");
        this.fenTrace.setFont(new Font("Courier", 0, 12));
        this.fenTrace.setEditable(false);
        this.add(this.fenTrace, "South");
        this.pack();
        this.setVisible(true);
    }

    public static void main(String[] args) {
        new FenAffichage();
    }

    private void afficherEtatAnalyse(int etatDepart, int unite, int etatArrivee, int action) {
        String mess = "depart=" + etatDepart + "  item=" + LexVin.images[unite] + "  arrivee=" + etatArrivee + "  action= " + action + "\n";
        this.fenTrace.append(mess);
    }

    public void notification(int etatDepart, int unite, int etatArrive, int action) {
        this.afficherEtatAnalyse(etatDepart, unite, etatArrive, action);
    }

    public void nouveauChar(char c) {
        this.fenEntree.append("" + c);
    }
}
