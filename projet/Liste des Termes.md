# Compilator for the language "Projet"

## Refresh the parser and lexical analyzer

Call this gradle task:
```bash
gradle refreshProject
```

The task will generate new classes with [project.g](https://github.com/istic-student/l3-cmpl/blob/master/projet/src/main/antlr/projet.g).

## Points de génération du fichier [project.g](https://github.com/istic-student/l3-cmpl/blob/master/projet/src/main/antlr/projet.g) 

Liste des termes avec leurs emplacements (classés selon l'ordre de la grammaire):

- Méthodes de vérifications :
```
 -> Cases 0 à 19
 -> Lignes 215 à 222
```
- UNITE :
```
 -> Cases 580 à 600
 -> Lignes 678 à 695 (FIN)
```

- UNITPROG :
```
 -> Cases 540 à 559
 -> Lignes 670 à 674
```

- UNITMODULE:
```
 -> Cases 560 à 579
 -> Lignes 674 à 678
```

- PARTIEDEF :
```
 -> Cases 500 à 519
 -> Lignes 649 à 653
```

- SPECIF :
```
 -> Cases 520 à 539
 -> Lignes 653 à 670
```
- CONSTS :
```
 -> Cases 20 à 39
 -> Lignes 222 à 230
```
- VARS :
```
 -> Cases 40 à 59
 -> Lignes 230 à 243
```
- TYPE :
```
 -> Cases 50 à 79
 -> Lignes 243 à 250
```
- DEPROCS :
```
 -> Cases 400 à 419
 -> Lignes 572 à 587
```
- DEPROC :
```
 -> Cases 420 à 439
 -> Lignes 587 à 614
```
- CORPS :
```
 -> Cases 440 à 459
 -> Lignes 614 à 631
 ```

- PF :
```
 -> Cases 460 à 479
 -> Lignes 631 à 640
```
- PM :
```
 -> Cases 480 à 499
 -> Lignes 640 à 649
```
- INSSI :
```
 -> Cases 360 à 379
 -> Lignes 533 à 546
```
- INSCOND :
```
 -> Cases 380 à 399
 -> Lignes 546 à 572
```
- BOUCLE :
```
 -> Cases 340 à 359
 -> Lignes 518 à 533
```
- LECTURE :
```
 -> Cases 240 à 259
 -> Lignes 355 à 399
```
- ECRITURE :
```
 -> Cases 260 à 279
 -> Lignes 399 à 413
```
- AFFOUAPPEL :
```
 -> Cases 280 à 299
 -> Lignes 413 à 478
```
- EFFIXES :
```
 -> Cases 300 à 319
 -> Lignes 478 à 486
```
- EFFMODS :
```
 -> Cases 320 à 339
 -> Lignes 486 à 518
```
- PRIMAIRE :
```
 -> Cases 100 à 119
 -> Lignes 267 à 307
```
- EXPRESSION :
```
 -> Cases 120 à 139
 -> Lignes 307 à 311
```
- EXP 1 :
```
 -> Cases 140 à 159
 -> Lignes 311 à 315
```
- EXP 2 :
```
 -> Cases 160 à 179
 -> Lignes 315 à 319
```
- EXP 3 :
```
 -> Cases 180 à 199
 -> Lignes 319 à 341
```
- EXP 4 :
```
 -> Cases 200 à 219
 -> Lignes 341 à 348
```
- EXP 5 :
```
 -> Cases 220 à 239
 -> Lignes 348 à 355
```
- VALEUR :
```
 -> Cases 80 à 99
 -> Lignes 250 à 267
```









