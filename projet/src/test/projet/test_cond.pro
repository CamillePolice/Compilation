programme exo2b :

{on lit une suite de valeurs entieres terminee par -1 et on denombre les valeurs comprises dans l'intervalle 0..9 ainsi que les valeurs comprises dans l'intervalle 10..20}

	var bool b1, b2, b3, b4;
debut
	b1 := vrai;
	b2 := faux;
	b3 := faux;
	b4 := vrai;
	cond
		b1: cond
		        b2: ecrire(2)
		    fcond,
		b3: ecrire(3)
		aut
		    cond
               b4: ecrire(4)
               aut ecrire(5)
            fcond
	fcond;
fin
