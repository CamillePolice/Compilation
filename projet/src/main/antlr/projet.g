// Grammaire du langage PROJET
// COMP L3  
// Anne Grazon, Veronique Masson
// il convient d'y inserer les appels a {PtGen.pt(k);}
// relancer Antlr apres chaque modification et raffraichir le projet Eclipse le cas echeant

// attention l'analyse est poursuivie apres erreur si l'on supprime la clause rulecatch

grammar projet;

options {
  language=Java; k=1;
 }

@header {           
import java.io.IOException;
import java.io.DataInputStream;
import java.io.FileInputStream;
} 


// partie syntaxique :  description de la grammaire //
// les non-terminaux doivent commencer par une minuscule


@members {

 
// variables globales et methodes utiles a placer ici
  
}
// la directive rulecatch permet d'interrompre l'analyse a la premiere erreur de syntaxe
@rulecatch {
catch (RecognitionException e) {reportError (e) ; throw e ; }}


unite  :   unitprog  EOF { PtGen.pt(100002); }
      |    unitmodule  EOF { PtGen.pt(100002); }
  ;
  
unitprog
  : 'programme' { PtGen.pt(100000); } ident ':'
     declarations
     corps { System.out.println("succes, arret de la compilation "); }
  ;
  
unitmodule
  : 'module' { PtGen.pt(100001); } ident ':'
     declarations   
  ;
  
declarations
  : partiedef? partieref? consts? vars? decprocs?
  ;
  
partiedef
  : 'def' ident { PtGen.pt(500); }  (',' ident { PtGen.pt(500); } )* ptvg
  ;
  
partieref: 'ref'  specif (',' specif)* ptvg
  ;
  
specif  : ident { PtGen.pt(520); }  ( 'fixe' '(' type { PtGen.pt(521); }  ( ',' type { PtGen.pt(521); }  )* ')' )?
                 ( 'mod'  '(' type { PtGen.pt(522); }  ( ',' type { PtGen.pt(522); }  )* ')' )? { PtGen.pt(523); }
  ;
  
consts  : 'const' ( ident  '=' valeur ptvg { PtGen.pt(20); } )+
  ;
  
vars  : 'var' ( type ident { PtGen.pt(40); } ( ','  ident  { PtGen.pt(40); } )* ptvg  )+ { PtGen.pt(41); }
  ;
  
type  : 'ent'  { PtGen.pt(60); }
  |     'bool' { PtGen.pt(61); }
  ;
  
decprocs: { PtGen.pt(400); } (decproc ptvg)+ { PtGen.pt(401); }
  ;
  
decproc :  'proc'  ident  { PtGen.pt(420); } parfixe? parmod? { PtGen.pt(421); } consts? vars? corps
  ;
  
ptvg  : ';'
  | 
  ;
  
corps : 'debut' instructions 'fin' { PtGen.pt(440); }
  ;
  
parfixe: 'fixe' '(' pf ( ';' pf)* ')'
  ;
  
pf  : type ident { PtGen.pt(460); } ( ',' ident  { PtGen.pt(460); })*
  ;

parmod  : 'mod' '(' pm ( ';' pm)* ')'
  ;
  
pm  : type ident { PtGen.pt(480); } ( ',' ident { PtGen.pt(480); } )*
  ;
  
instructions
  : instruction ( ';' instruction)*
  ;
  
instruction
  : inssi
  | inscond
  | boucle
  | lecture
  | ecriture
  | affouappel
  |
  ;
  
inssi : 'si' expression { PtGen.pt(341); } 'alors' instructions ('sinon' { PtGen.pt(360); }  instructions)? 'fsi' { PtGen.pt(361); }
  ;
  
inscond : 'cond' { PtGen.pt(380); } expression { PtGen.pt(341); } ':' instructions
          (',' { PtGen.pt(381); }  expression { PtGen.pt(341); } ':' instructions)*
          ('aut' { PtGen.pt(381); } instructions | { PtGen.pt(382); } )
          'fcond' { PtGen.pt(383); }
  ;
  
boucle  : 'ttq' { PtGen.pt(340); } expression { PtGen.pt(341); } 'faire' instructions 'fait' { PtGen.pt(342); }
  ;
  
lecture: 'lire' '(' ident { PtGen.pt(240); } ( ',' ident { PtGen.pt(240); } )*  ')'
  ;
  
ecriture: 'ecrire' '(' expression  { PtGen.pt(260); } ( ',' expression { PtGen.pt(260); } )* ')'
   ;
  
affouappel
  : ident { PtGen.pt(280); }  (    ':=' expression { PtGen.pt(281); }
            |  { PtGen.pt(282); }  (effixes (effmods)?  )? { PtGen.pt(283); }
           )
  ;
  
effixes : '(' (expression { PtGen.pt(300); } (',' expression { PtGen.pt(300); } )* )? ')'
  ;
  
effmods :'(' (ident { PtGen.pt(320); }  (',' ident { PtGen.pt(320); }  )*)? ')'
  ; 
  
expression: (exp1) ('ou' { PtGen.pt(2); } exp1 { PtGen.pt(2); } { PtGen.pt(120); })*
  ;
  
exp1  : exp2 ('et' { PtGen.pt(2); } exp2 { PtGen.pt(2); } { PtGen.pt(140); } )*
  ;
  
exp2  : 'non' exp2 { PtGen.pt(2); } { PtGen.pt(160); }
  | exp3
  ;
  
exp3  : exp4 
  ( '='  { PtGen.pt(1); } exp4  { PtGen.pt(1); } { PtGen.pt(181); } { PtGen.pt(180); }
  | '<>' { PtGen.pt(1); } exp4  { PtGen.pt(1); } { PtGen.pt(182); } { PtGen.pt(180); }
  | '>'  { PtGen.pt(1); } exp4  { PtGen.pt(1); } { PtGen.pt(183); } { PtGen.pt(180); }
  | '>=' { PtGen.pt(1); } exp4  { PtGen.pt(1); } { PtGen.pt(184); } { PtGen.pt(180); }
  | '<'  { PtGen.pt(1); } exp4  { PtGen.pt(1); } { PtGen.pt(185); } { PtGen.pt(180); }
  | '<=' { PtGen.pt(1); } exp4  { PtGen.pt(1); } { PtGen.pt(186); } { PtGen.pt(180); }
  ) ?
  ;
  
exp4  : exp5 
        ('+'  { PtGen.pt(1); } exp5  { PtGen.pt(1); } { PtGen.pt(200); }
        |'-'  { PtGen.pt(1); } exp5  { PtGen.pt(1); } { PtGen.pt(201); }
        )*
  ;
  
exp5  : primaire 
        (    '*'  { PtGen.pt(1); } primaire { PtGen.pt(1); } { PtGen.pt(220); }
          | 'div' { PtGen.pt(1); } primaire { PtGen.pt(1); } { PtGen.pt(221); }
        )*
  ;
  
primaire: valeur { PtGen.pt(100); }
  | ident  { PtGen.pt(101); }
  | '(' expression ')'
  ;

valeur  : nbentier { PtGen.pt(80); }
  | '+' nbentier { PtGen.pt(80); }
  | '-' nbentier { PtGen.pt(81); }
  | 'vrai' { PtGen.pt(82); }
  | 'faux' { PtGen.pt(83); }
  ;

// partie lexicale  : cette partie ne doit pas etre modifiee  //
// les unites lexicales de ANTLR doivent commencer par une majuscule
// attention : ANTLR n'autorise pas certains traitements sur les unites lexicales, 
// il est alors ncessaire de passer par un non-terminal intermediaire 
// exemple : pour l'unit lexicale INT, le non-terminal nbentier a du etre introduit
 
      
nbentier  :   INT { UtilLex.valNb = Integer.parseInt($INT.text);}; // mise a jour de valNb

ident : ID  { UtilLex.traiterId($ID.text); } ; // mise a jour de numId
     // tous les identificateurs seront places dans la table des identificateurs, y compris le nom du programme ou module
     // la table des symboles n'est pas geree au niveau lexical
        
  
ID  :   ('a'..'z'|'A'..'Z')('a'..'z'|'A'..'Z'|'0'..'9'|'_')* ; 
     
// zone purement lexicale //

INT :   '0'..'9'+ ;
WS    :   (' '|'\t' |'\r')+ {skip();} ; // definition des "espaces"
LIGNE :   '\n' {UtilLex.incrementeLigne();skip();};

COMMENT
  :  '\{' (.)* '\}' {skip();}   // toute suite de caracteres entouree d'accolades est un commentaire
  |  '#' ~( '\r' | '\n' )* {skip();}  // tout ce qui suit un caractere diese sur une ligne est un commentaire
  ;

// commentaires sur plusieurs lignes
ML_COMMENT    :   '/*' (options {greedy=false;} : .)* '*/' {$channel=HIDDEN;}
    ;	   



	   
