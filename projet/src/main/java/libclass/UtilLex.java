package libclass;
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

public class UtilLex {
    public static String nomSource;
    public static int numId;
    public static int valNb;
    private static int numLigne;
    private static final int MAXID = 2000;
    private static int nbId;
    private static String[] identificateurs;

    public UtilLex() {
    }

    private static void arret() {
        System.out.println("erreur, arret de la compilation");
        System.exit(0);
    }

    public static void messErr(String message) {
        System.out.println("erreur, ligne numero : " + numLigne);
        System.out.println(message);
        arret();
    }

    public static void initialisation() {
        nbId = 0;
        identificateurs = new String[2000];
        numId = 0;
        valNb = 0;
        numLigne = 1;
    }

    private static int chercherId(String id) {
        int indice;
        for(indice = 0; indice < nbId && !id.equals(identificateurs[indice]); ++indice) {
            ;
        }

        if(indice == nbId) {
            if(nbId == 2000) {
                messErr("Debordement de la table des identificateurs");
            }

            identificateurs[nbId] = id;
            ++nbId;
        }

        return indice;
    }

    public static String repId(int i) {
        if(i >= nbId || i < 0) {
            messErr("repid sur num ident errone");
        }

        return identificateurs[i];
    }

    public static void traiterId(String id) {
        numId = chercherId(id.toLowerCase());
    }

    public static void incrementeLigne() {
        ++numLigne;
    }

    private static void afftabIdentificateurs() {
        System.out.println("       numero           libelle     ");
        System.out.println("      |--------------|--------------");

        for(int i = 0; i < nbId; ++i) {
            Ecriture.ecrireInt(i, 6);
            if(identificateurs[i] == null) {
                System.out.println(" reference NULL");
            } else {
                System.out.println(" " + identificateurs[i]);
            }
        }

        System.out.println();
    }
}
