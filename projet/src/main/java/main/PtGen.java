package main;

// classe de mise en oeuvre du compilateur
// =======================================
// (verifications semantiques + production du code objet)

import libclass.Ecriture;
import libclass.UtilLex;

import java.util.Arrays;
import java.util.OptionalInt;

public class PtGen {


    // constantes manipulees par le compilateur
    // ----------------------------------------

    private static final int

            // taille max de la table des symboles
            MAXSYMB = 300,

    // codes MAPILE :
    TEMP = 0, RESERVER = 1, EMPILER = 2, CONTENUG = 3, AFFECTERG = 4, OU = 5, ET = 6, NON = 7, INF = 8,
            INFEG = 9, SUP = 10, SUPEG = 11, EG = 12, DIFF = 13, ADD = 14, SOUS = 15, MUL = 16, DIV = 17,
            BSIFAUX = 18, BINCOND = 19, LIRENT = 20, LIREBOOL = 21, ECRENT = 22, ECRBOOL = 23,
            ARRET = 24, EMPILERADG = 25, EMPILERADL = 26, CONTENUL = 27, AFFECTERL = 28,
            APPEL = 29, RETOUR = 30,

    // codes des valeurs vrai/faux
    VRAI = 1, FAUX = 0,

    // types permis :
    ENT = 1, BOOL = 2, NEUTRE = 3,

    // cat�gories possibles des identificateurs :
    CONSTANTE = 1, VARGLOBALE = 2, VARLOCALE = 3, PARAMFIXE = 4, PARAMMOD = 5, PROC = 6,
            DEF = 7, REF = 8, PRIVEE = 9,

    //valeurs possible du vecteur de translation
    TRANSDON = 1, TRANSCODE = 2, REFEXT = 3;


    // utilitaires de controle de type
    // -------------------------------
    // autres variables fournies
    // -------------------------
    public static String trinome = "DELORME_POLICE_LOUSTAU"; // MERCI de renseigner ici un nom pour le trinome, constitue de exclusivement de lettres
    private static TPileRep pileRep;

    // pile pour gerer les chaines de reprise et les branchements en avant
    // -------------------------------------------------------------------
    // it = indice de remplissage de tabSymb
    // bc = bloc courant (=1 si le bloc courant est le programme principal)
    private static int it, bc;


    // production du code objet en memoire
    // -----------------------------------

    private static ProgObjet po;
    // descripteur associe a un programme objet
    private static Descripteur desc;

    private static int tCour; // type de l'expression compilee
    private static int vCour; // valeur de l'expression compilee le cas echeant

    // D�finition de la table des symboles
    //
    private static EltTabSymb[] tabSymb = new EltTabSymb[MAXSYMB + 1];

    // Variables utilisées non fournies
    static int tabSymb_Cour;
    static int tabSymb_AffectCour;
    static int tabSymb_nbVarGlobales = 0;

    private static int varIt;
    private static int tVar;
    private static int cVar;
    private static int varIdent;

    private static boolean inProc;
    private static int nbParamsProc;
    private static int indexPriveProc;
    private static int identProc;

    private static int nbProcedures;
    private static int debutProc;
    private static int nbInProcParam;
    private static String nomProc;
    private static int nbParamsRef;


    private static void verifEnt() {
        if (tCour != ENT)
            UtilLex.messErr("expression entiere attendue");
    }

    private static void verifBool() {
        if (tCour != BOOL)
            UtilLex.messErr("expression booleenne attendue");
    }

    // COMPILATION SEPAREE
    //
    // -------------------
    // modification du vecteur de translation associe au code produit
    // + incrementation attribut nbTransExt du descripteur
    // NB: effectue uniquement si c'est une reference externe ou si on compile un module
    private static void modifVecteurTrans(int valeur) {
        if (valeur == REFEXT || desc.getUnite().equals("module")) {
            po.vecteurTrans(valeur);
            desc.incrNbTansExt();
        }
    }

    // utilitaire de recherche de l'ident courant (ayant pour code UtilLex.numId) dans tabSymb
    // rend en resultat l'indice de cet ident dans tabSymb (O si absence)
    private static int presentIdent(int binf) {
        int i = it;
        while (i >= binf && tabSymb[i].code != UtilLex.numId)
            i--;
        if (i >= binf)
            return i;
        else
            return 0;
    }

    private OptionalInt rechercher(int value, int[] tab) {
        return Arrays.stream(tab).parallel().filter(v -> v == value).findFirst();
    }

    // utilitaire de placement des caracteristiques d'un nouvel ident dans tabSymb
    //
    private static void placeIdent(int c, int cat, int t, int v) {
        if (it == MAXSYMB)
            UtilLex.messErr("debordement de la table des symboles");
        it = it + 1;
        tabSymb[it] = new EltTabSymb(c, cat, t, v);
    }

    // utilitaire d'affichage de la table des symboles
    //
    private static void afftabSymb() {
        System.out.println("       code            categorie      type    info");
        System.out.println("      |--------------|--------------|-------|----");
        for (int i = 1; i <= it; i++) {
            if (i == bc) {
                System.out.print("bc=");
                Ecriture.ecrireInt(i, 3);
            } else if (i == it) {
                System.out.print("it=");
                Ecriture.ecrireInt(i, 3);
            } else
                Ecriture.ecrireInt(i, 6);
            if (tabSymb[i] == null)
                System.out.println(" r�f�rence NULL");
            else
                System.out.println(" " + tabSymb[i]);
        }
        System.out.println();
    }


    // initialisations A COMPLETER SI BESOIN
    // -------------------------------------

    public static void initialisations() {

        // indices de gestion de la table des symboles
        it = 0;
        bc = 1;

        // pile des reprises pour compilation des branchements en avant
        pileRep = new TPileRep();
        // programme objet = code Mapile de l'unite en cours de compilation
        po = new ProgObjet();
        // COMPILATION SEPAREE: desripteur de l'unite en cours de compilation
        desc = new Descripteur();

        // initialisation necessaire aux attributs lexicaux
        UtilLex.initialisation();

        // initialisation du type de l'expression courante
        tCour = NEUTRE;

        // Initialisation des variables locales
        tabSymb_nbVarGlobales = 0;
        tabSymb_Cour = 0;
        tabSymb_AffectCour = 0;

        inProc = false;
        varIt = 0;
        nbParamsProc = 0;
        indexPriveProc = 0;

        tVar = 0;
        cVar = 0;

        debutProc = 0;
        nbInProcParam = 0;
        identProc = 0;
        nomProc = "";
        nbProcedures = 0;

        nbParamsRef = 0;
    } // initialisations

    // code des points de generation A COMPLETER
    // -----------------------------------------
    public static void pt(int numGen) {

        System.out.println("Point de génération: " + numGen);

        switch (numGen) {
            case 0:
                initialisations();
                break;
            //*************** Méthodes de vérifications de type ***************//
            case 1:
                verifEnt();
                break;
            case 2:
                verifBool();
                break;
            //*************** CONSTS ***************//
            case 20:
                if (presentIdent(1) == 0) {
                    placeIdent(UtilLex.numId, CONSTANTE, tCour, vCour);
                    break;
                }
                UtilLex.messErr("Identifiant de constante déjà déclaré, numid=" + UtilLex.numId);
                break;
            //*************** VARS ***************//
            case 40:
                if (presentIdent(1) != 0)
                    UtilLex.messErr("Identifiant de variable globale déjà déclaré, numid=" + UtilLex.numId);

                placeIdent(UtilLex.numId, inProc ? VARLOCALE : VARGLOBALE, tCour, tabSymb_nbVarGlobales);
                tabSymb_nbVarGlobales++;
                // Incrémente de 1 le nombre de variables globales dans le programme
                // Si on est pas dans une procédure
                if (!inProc)
                    desc.setTailleGlobaux(desc.getTailleGlobaux() + 1);
                break;
            case 41:
                if (desc.getUnite().equals("programme")) {
                    po.produire(RESERVER);
                    po.produire(tabSymb_nbVarGlobales);
                }
                tabSymb_nbVarGlobales = 0;
                break;
            //*************** TYPE ***************//
            case 60:
                tCour = ENT;
                break;
            case 61:
                tCour = BOOL;
                break;
            //*************** VALEUR ***************//
            case 80:
                tCour = ENT;
                vCour = UtilLex.valNb;
                break;
            case 81:
                tCour = ENT;
                vCour = UtilLex.valNb * (-1);
                break;
            case 82:
                tCour = BOOL;
                vCour = VRAI;
                break;
            case 83:
                tCour = BOOL;
                vCour = FAUX;
                break;
            //*************** PRIMAIRE ***************//
            case 100:
                po.produire(EMPILER);
                po.produire(vCour);
                break;
            case 101:
                // On vérifie si l'ident renseigné est présent dans tabSymb
                tabSymb_Cour = presentIdent(1);
                if (tabSymb_Cour == 0) {
                    UtilLex.messErr("Ident -> " + UtilLex.numId + " non déclaré dans la table des symboles");
                } else {
                    // On récupère son type
                    tCour = tabSymb[tabSymb_Cour].type;

                    // Puis sa catégorie entre les catégories existantes
                    switch (tabSymb[tabSymb_Cour].categorie) {
                        case VARGLOBALE:
                            po.produire(CONTENUG);
                            po.produire(tabSymb[tabSymb_Cour].info);
                            modifVecteurTrans(TRANSDON);
                            break;
                        case CONSTANTE:
                            po.produire(EMPILER);
                            po.produire(tabSymb[tabSymb_Cour].info);
                            break;
                        case VARLOCALE:
                        case PARAMFIXE:
                            po.produire(CONTENUL);
                            po.produire(tabSymb[tabSymb_Cour].info);
                            po.produire(0);
                            break;
                        case PARAMMOD:
                            po.produire(CONTENUL);
                            po.produire(tabSymb[tabSymb_Cour].info);
                            po.produire(1);
                            break;
                        default:
                            UtilLex.messErr("Catégorie -> " + tabSymb[tabSymb_Cour].categorie + " invalide (ident erroné)");
                            break;
                    }
                }
                break;
            //*************** EXPRESSION ***************//
            case 120:
                po.produire(OU);
                break;
            //*************** EXP 1 ***************//
            case 140:
                po.produire(ET);
                break;
            //*************** EXP 2 ***************//
            case 160:
                po.produire(NON);
                break;
            //*************** EXP 3 ***************//
            case 180:
                tCour = BOOL;
                break;
            case 181:
                po.produire(EG);
                break;
            case 182:
                po.produire(DIFF);
                break;
            case 183:
                po.produire(SUP);
                break;
            case 184:
                po.produire(SUPEG);
                break;
            case 185:
                po.produire(INF);
                break;
            case 186:
                po.produire(INFEG);
                break;
            //*************** EXP 4 ***************//
            case 200:
                po.produire(ADD);
                break;
            case 201:
                po.produire(SOUS);
                break;
            //*************** EXP 5 ***************//
            case 220:
                po.produire(MUL);
                break;
            case 221:
                po.produire(DIV);
                break;
            //*************** LECTURE ***************//
            case 240:
                // On vérifie si l'ident renseigné est présent dans tabSymb
                tabSymb_Cour = presentIdent(1);
                if (tabSymb_Cour == 0) {
                    UtilLex.messErr("Erreur. Ident -> " + UtilLex.numId + " non déclaré dans la table des symboles");
                }
                // affTabSymb()
                // On vérifie si ce qu'on obtient est bien une variable et non une constante
                // étant donné qu'on lit une variable saisit par l'utilisateur
                if (tabSymb[tabSymb_Cour].categorie == CONSTANTE)
                    UtilLex.messErr("Erreur, une variable est requise ici");
                switch (tabSymb[tabSymb_Cour].type) {
                    case BOOL:
                        po.produire(LIREBOOL);
                        break;
                    case ENT:
                        po.produire(LIRENT);
                        break;
                    default:
                        UtilLex.messErr("Erreur, Identificateur -> " + tabSymb[tabSymb_Cour].type + "erroné");
                        break;
                }
                switch (tabSymb[tabSymb_Cour].categorie) {
                    case VARGLOBALE:
                        po.produire(AFFECTERG);
                        po.produire(tabSymb[tabSymb_Cour].info);
                        modifVecteurTrans(TRANSDON);
                        break;
                    case VARLOCALE:
                        po.produire(AFFECTERL);
                        po.produire(tabSymb[tabSymb_Cour].info);
                        po.produire(0);
                        break;
                    case PARAMMOD:
                        po.produire(AFFECTERL);
                        po.produire(tabSymb[tabSymb_Cour].info);
                        po.produire(1);
                        break;
                    default:
                        UtilLex.messErr("Erreur -> Mauvaise catégorie.");
                        break;
                }
                break;
            //*************** ECRITURE ***************//
            case 260:
                switch (tCour) {
                    case BOOL:
                        po.produire(ECRBOOL);
                        break;
                    case ENT:
                        po.produire(ECRENT);
                        break;
                    default:
                        UtilLex.messErr("Erreur, Type -> " + vCour + " erroné");
                        break;
                }
                break;
            //*************** AFFOUAPPEL ***************//
            case 280:
                // On vérifie si l'ident renseigné est présent dans tabSymb
                tabSymb_AffectCour = presentIdent(1);
                if (tabSymb_AffectCour == 0) {
                    UtilLex.messErr("Erreur. Ident -> " + UtilLex.numId + " non déclaré dans la table des symboles");
                }
                // On vérifie si ce qu'on obtient est bien une variable et non une constante
                // étant donné qu'on lit une variable saisit par l'utilisateur
                if (tabSymb[tabSymb_AffectCour].categorie == CONSTANTE) {
                    UtilLex.messErr("Erreur, une variable est requise ici");
                }
                tCour = tabSymb[tabSymb_AffectCour].type;
                tVar = tabSymb[tabSymb_AffectCour].type;
                cVar = tabSymb[tabSymb_AffectCour].categorie;
                break;
            case 281:
                if (tVar == ENT)
                    verifEnt();
                else
                    verifBool();

                switch (cVar) {
                    case VARGLOBALE:
                        po.produire(AFFECTERG);
                        po.produire(tabSymb[tabSymb_AffectCour].info);
                        modifVecteurTrans(TRANSDON);
                        break;
                    case VARLOCALE:
                        po.produire(AFFECTERL);
                        po.produire(tabSymb[tabSymb_AffectCour].info);
                        po.produire(0);
                        break;
                    case PARAMMOD:
                        po.produire(AFFECTERL);
                        po.produire(tabSymb[tabSymb_AffectCour].info);
                        po.produire(1);
                        break;
                    default:
                        UtilLex.messErr("Erreur -> L'affectation a echoué");
                        break;
                }
                break;
            // Appel Procédure
            case 282:
                identProc = presentIdent(1);
                if (identProc == 0)
                    UtilLex.messErr("Identifiant non trouvé");
                if (tabSymb[identProc].categorie == PROC) {
                    debutProc = identProc + 2;
                    nbInProcParam = 0;
                } else {
                    UtilLex.messErr("L'identifiant n'est pas une procédure.");
                }
                break;
            // Production de l'appel
            case 283:
                //afftabSymb();
                if (tabSymb[identProc + 1].info <= nbInProcParam) {
                    po.produire(APPEL);
                    po.produire((tabSymb[identProc].info));
                    if (tabSymb[identProc + 1].categorie == REF)
                        modifVecteurTrans(REFEXT);
                    else
                        modifVecteurTrans(TRANSCODE);
                    po.produire(nbInProcParam);
                } else {
                    UtilLex.messErr("Erreur -> Nombre de paramètres dans la procédure erroné.");
                }
                break;
            //*************** EFFIXES ***************//
            // Ajout des paramètres fixes
            case 300:
                if (tabSymb[debutProc + nbInProcParam].categorie != PARAMFIXE
                        || tabSymb[debutProc + nbInProcParam].type != tCour) {
                    /*afftabSymb();
                    System.out.println(nbInProcParam);
                    System.out.println(debutProc);
                    System.out.println(debutProc + nbInProcParam);
                    System.out.println(tCour);*/
                    UtilLex.messErr("Erreur -> Paramètre fixe erroné (Ajout param fixe)");
                }
                nbInProcParam++;
                break;
            //*************** EFFMODS ***************//
            // Ajout des paramètres modulaires
            case 320:
                int tempo = presentIdent(1);
                if (tempo == 0)
                    UtilLex.messErr("Identifiant non trouvé");
                afftabSymb();
                System.out.println(nbInProcParam);
                System.out.println(debutProc);
                System.out.println(tCour);
                if (tabSymb[debutProc + nbInProcParam].categorie == PARAMMOD
                        && tabSymb[debutProc + nbInProcParam].type == tabSymb[tempo].type) {
                    switch (tabSymb[debutProc + nbInProcParam].categorie) {
                        case VARGLOBALE:
                            po.produire(EMPILERADG);
                            po.produire(tabSymb[debutProc + nbInProcParam].info);
                            modifVecteurTrans(TRANSDON);
                            break;
                        case VARLOCALE:
                        case PARAMFIXE:
                            po.produire(EMPILERADL);
                            po.produire(tabSymb[debutProc + nbInProcParam].info);
                            po.produire(0);
                            break;
                        case PARAMMOD:
                            po.produire(EMPILERADL);
                            po.produire(tabSymb[debutProc + nbInProcParam].info);
                            po.produire(1);
                            break;
                        default:
                            UtilLex.messErr("Erreur -> Catégorie du paramètre erronée");
                    }
                } else {
                    UtilLex.messErr("Erreur -> Paramètre modifiable erroné. (Ajout param mod)");
                }
                nbInProcParam++;
                break;
            //*************** BOUCLE ***************//
            case 340:
                pileRep.empiler(po.getIpo() + 1);
                break;
            // Production BSIFAUX (utilisation dans plusieurs autres endroits)
            case 341:
                po.produire(BSIFAUX);
                po.produire(TEMP);
                modifVecteurTrans(TRANSCODE);
                pileRep.empiler(po.getIpo());
                break;
            case 342:
                po.produire(BINCOND);
                po.modifier(pileRep.depiler(), po.getIpo() + 2);
                po.produire(pileRep.depiler());
                modifVecteurTrans(TRANSCODE);
                break;
            //*************** INSSI ***************//
            // Sinon => On produit le bindond 0 (on ne sait pas quoi mettre)
            //          on empile l'adresse dans pileRep et on résout le si
            case 360:
                po.produire(BINCOND);
                po.produire(TEMP);
                modifVecteurTrans(TRANSCODE);
                po.modifier(pileRep.depiler(), po.getIpo() + 1);
                pileRep.empiler(po.getIpo());
                break;
            // Fsi => On résout les BINCOND et BSIFAUX (dépiler pileRep)
            case 361:
                po.modifier(pileRep.depiler(), po.getIpo() + 1);
                break;
            //*************** INSCOND ***************//
            case 380:
                pileRep.empiler(TEMP);
                break;
            case 381:
                po.produire(BINCOND);
                po.modifier(pileRep.depiler(), po.getIpo() + 2);
                po.produire(pileRep.depiler());
                modifVecteurTrans(TRANSCODE);
                pileRep.empiler(po.getIpo());
                break;
            case 382:
                po.modifier(pileRep.depiler(), po.getIpo() + 1);
                break;
            // Chaîne de résolutions
            case 383:
                int premierBINCOND = pileRep.depiler();
                int suivantBINCOND = po.getElt(premierBINCOND);
                int tempBINDCOND = 0;

                po.modifier(premierBINCOND, po.getIpo() + 1);
                while (suivantBINCOND != 0) {
                    tempBINDCOND = po.getElt(suivantBINCOND);
                    po.modifier(suivantBINCOND, po.getIpo() + 1);
                    suivantBINCOND = tempBINDCOND;
                }
                break;
            //*************** DECPROCS ***************//
            // Début de déclaration des procédures
            case 400:
                if (desc.getUnite().equals("programme")) {
                    po.produire(BINCOND);
                    po.produire(TEMP);
                    modifVecteurTrans(TRANSCODE);
                    pileRep.empiler(po.getIpo());
                }
                break;
            case 401:
                if (desc.getNbDef() > nbProcedures)
                    UtilLex.messErr("Erreur -> Nombre de procèdures erroné");
                if (desc.getUnite().equals("programme"))
                    po.modifier(pileRep.depiler(), po.getIpo() + 1);
                for (int i = desc.getNbDef(); i > 0; i--) {
                    if (desc.getDefAdPo(i) == -1)
                        UtilLex.messErr("Erreur, procédure déclarée mais pas programmée");
                }
                break;
            //*************** DECPROC ***************//
            case 420:
                varIt = 0;
                inProc = true;
                nbProcedures++;
                placeIdent(UtilLex.numId, PROC, NEUTRE, po.getIpo() + 1);

                bc = it + 1;
                indexPriveProc = it;
                nbParamsProc = 0;

                nomProc = UtilLex.repId(UtilLex.numId);
                int idDef = desc.presentDef(nomProc);
                if (idDef != 0) {
                    desc.modifDefAdPo(idDef, po.getIpo() + 1);
                    placeIdent(-1, DEF, NEUTRE, varIt);
                } else {
                    placeIdent(-1, PRIVEE, NEUTRE, varIt);
                }
                break;
            case 421:
                tabSymb[indexPriveProc + 1].info = varIt;
                int idDefv2 = desc.presentDef(nomProc);
                if (idDefv2 != 0)
                    desc.modifDefAdPo(idDefv2, varIt);

                varIt = varIt + 2;
                break;
            //*************** CORPS ***************//
            case 440:
                if (inProc) {
                    inProc = false;
                    po.produire(RETOUR);
                    po.produire(nbParamsProc);
                    while (it >= bc
                            && (tabSymb[it].categorie == CONSTANTE
                            || tabSymb[it].categorie == VARLOCALE)) {
                        it--;
                    }
                    int ident = it;
                    while (ident >= bc) {
                        tabSymb[ident--].code = -1;
                    }
                }
                break;
            //*************** PF (Paramètres fixes) ***************//
            case 460:
                if (presentIdent(bc) == 0) {
                    placeIdent(UtilLex.numId, PARAMFIXE, tCour, varIt++);
                    nbParamsProc++;
                } else {
                    UtilLex.messErr("Erreur -> Paramètre déjà existant dans la procédure (Fix)");
                }
                break;
            //*************** PM (Paramètres modulaires) ***************//
            case 480:
                if (presentIdent(bc) == 0) {
                    placeIdent(UtilLex.numId, PARAMMOD, tCour, varIt++);
                    nbParamsProc++;
                } else {
                    UtilLex.messErr("Erreur -> Paramètre déjà existant dans la procédure (Mod)");
                }
                break;
            //*************** PARTIEDEF ***************//
            case 500:
                desc.ajoutDef(UtilLex.repId(UtilLex.numId));
                break;
            //*************** SPECIF ***************//
            case 520:
                desc.ajoutRef(UtilLex.repId(UtilLex.numId));
                placeIdent(UtilLex.numId, PROC, NEUTRE, desc.presentRef(UtilLex.repId(UtilLex.numId)));
                placeIdent(-1, REF, NEUTRE, TEMP);
                break;
            case 521:
                placeIdent(-1, PARAMFIXE, tCour, nbParamsRef++);
                break;
            case 522:
                placeIdent(-1, PARAMMOD, tCour, nbParamsRef++);
                break;
            case 523:
                int presentId = desc.presentRef(UtilLex.repId(UtilLex.numId));
                if (presentId != 0)
                    desc.modifRefNbParam(presentId, nbParamsRef);
                else
                    UtilLex.messErr("Erreur, référence non présente");
                // afftabSymb();
                tabSymb[presentIdent(1) + 1].info = nbParamsRef;
                nbParamsRef = 0;
                // afftabSymb();
                break;
            //*************** UNITEPROG ***************//
            case 100000:
                desc.setUnite("programme");
                break;
            //*************** UNITEMODULE ***************//
            case 100001:
                desc.setUnite("module");
                break;
            //*************** UNITE ***************//
            case 100002:
                afftabSymb();
                if (desc.getUnite().equals("programme"))
                    po.produire(ARRET);
                desc.ecrireDesc(UtilLex.nomSource);
                po.constObj();
                po.constGen();
                break;
            default:
                System.out
                        .println("Point de generation non prévu dans votre liste");
                break;

        }
    }
}


