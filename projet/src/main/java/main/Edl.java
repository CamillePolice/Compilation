package main;

import libclass.Ecriture;
import libclass.Lecture;

import java.io.InputStream;
import java.io.OutputStream;


public class Edl {
	
	// nombre max de modules, taille max d'un code objet d'une unite
	static final int MAXMOD = 5, MAXOBJ = 1000;
	// nombres max de references externes (REF) et de points d'entree (DEF)
	// pour une unite
	private static final int MAXREF = 10, MAXDEF = 10;
	
	// typologie des erreurs
	private static final int FATALE = 0, NONFATALE = 1;
	
	// valeurs possibles du vecteur de translation
	private static final int TRANSDON=1,TRANSCODE=2,REFEXT=3;
	
	// table de tous les descripteurs concernes par l'edl
	static Descripteur[] tabDesc = new Descripteur[MAXMOD + 1];
	
	// declarations de variables A COMPLETER SI BESOIN
	static int ipo, nMod, nbErr;
	static String nomProg;
	static String[] nomMods = new String[MAXMOD + 1];
	static int[] transDon = new int[MAXMOD + 1];
	static int[] transCode = new int[MAXMOD + 1];
	static Descripteur.EltDef[] dicoDef = new Descripteur.EltDef[(MAXMOD +1)*MAXDEF];
	static int nbDef = 0;
	static int[][] adFinale = new int[MAXMOD+1][MAXDEF+1];

	// utilitaire de traitement des erreurs
	// ------------------------------------
	static void erreur(int te, String m) {
		System.out.println(m);
		if (te == FATALE) {
			System.out.println("ABANDON DE L'EDITION DE LIENS");
			System.exit(1);
		}
		nbErr = nbErr + 1;
	}

	// utilitaire de remplissage de la table des descripteurs tabDesc
	// --------------------------------------------------------------
	static void lireDescripteurs() {
		String s;
		System.out.println("les noms doivent etre fournis sans suffixe");
		System.out.print("nom du programme : ");
		s = Lecture.lireString();
		tabDesc[0] = new Descripteur();
		tabDesc[0].lireDesc(s);
		if (!tabDesc[0].getUnite().equals("programme"))
			erreur(FATALE, "programme attendu");
		nomProg = s;
		
		nMod = 0;
		while (!s.equals("") && nMod < MAXMOD) {
			System.out.print("nom de module " + (nMod + 1)
					+ " (RC si termine) ");
			s = Lecture.lireString();
			if (!s.equals("")) {
				nMod = nMod + 1;
				tabDesc[nMod] = new Descripteur();
				tabDesc[nMod].lireDesc(s);
				if (!tabDesc[nMod].getUnite().equals("module"))
					erreur(FATALE, "module attendu");
			}
		}
	}

	
	static void constMap() {
		// f2 = fichier ex�cutable .map construit
		OutputStream f2 = Ecriture.ouvrir(nomProg + ".map");
		if (f2 == null)
			erreur(FATALE, "cr�ation du fichier " + nomProg
					+ ".map impossible");
		// pour construire le code concat�n� de toutes les unit�s
		int[] po = new int[(nMod + 1) * MAXOBJ + 1];
		
		ipo=1;
		
		for( int i = 0; i<=nMod; i++) {
			//ouverture des moules
			InputStream module = Lecture.ouvrir(nomMods[i] + ".obj");
			if (module == null) {
				erreur(FATALE, nomMods[i] + ".obj impossible a ouvrir");
			}

			int[] vTrans = new int[MAXOBJ + 1];

			//on récupère les translations que l'on doit faire
			for (int j = 0; j<tabDesc[i].getNbTransExt(); j++) {
				int it = Lecture.lireInt(module) + transCode[i];
				vTrans[it] = Lecture.lireIntln(module);
			}

			int ref = 1;
			//on ajoute le code des modules au code principal avec les modifications des translations
			for (int j = 0; j<tabDesc[j].getTailleCode(); j++) {
				po[ipo] = Lecture.lireIntln(module);
				switch (vTrans[ipo]) {
					case TRANSDON:
						po[ipo] += transDon[i];
						break;
					case TRANSCODE:
						po[ipo] += transCode[i];
						break;
					case REFEXT:
						po[ipo] = adFinale[i][ref];
						ref++;
						break;
				}
				ipo++;
			}
			Lecture.fermer(module);
		}
		//on enlève la dernière incrementation car plus de module à ajouter
		ipo--;

		//mise à jour du nombre total de variables globales pour le RESERVER
		po[2] = transDon[nMod] + tabDesc[nMod].getTailleGlobaux();

		//on vient écrire les modifications sur le fichier
		for (int i = 1; i<ipo; i++) {
			Ecriture.ecrireStringln(f2, ""+po[i]);
		}

		Ecriture.fermer(f2);
		// cr�ation du fichier en mn�monique correspondant
		Mnemo.creerFichier(ipo, po, nomProg + ".ima");
	}

	public static void main(String argv[]) {
		System.out.println("EDITEUR DE LIENS / PROJET LICENCE");
		System.out.println("---------------------------------");
		System.out.println("");
		nbErr = 0;
		
		// Phase 1 de l'edition de liens
		// -----------------------------
		lireDescripteurs();        // lecture des descripteurs à completer si besoin
		transDon[0] = 0;
		transCode[0] = 0;

		for (int i = 1;i<=nMod; i++) {
			//remplissage de transDon, transCode et dicoRef
			transDon[i] = transDon[i-1] + tabDesc[i-1].getTailleGlobaux();
			transCode[i] = transCode[i-1] + tabDesc[i-1].getTailleCode();

			//on regarde toutes les def et on fait l'union
			for (int j = i; j<=tabDesc[i].getNbDef(); j++) {
				Descripteur courante = tabDesc[i];
				//on regarde si la def est déjà définie
				for (int k = 0; k<nbDef; k++) {
					/*if (dicoDef[k].nomProc.equals(courante.getDefNomProc(j))) {
						erreur(FATALE, courante.getDefNomProc(j) + "déjà définie");
					}*/
				}
				courante.modifDefAdPo(j,transCode[i]);
				//ajout de la def courante
				/*dicoDef[nbDef].nomProc = courante.getDefNomProc(j);
				dicoDef[nbDef].adPo = courante.getDefAdPo(j);
				dicoDef[nbDef].nbParam = courante.getDefNbParam(j);*/
				nbDef++;
			}
		}

		//mise à jour de l'adresse finale de la def
		/*for (int i = 0; i<=nMod; i++) {
			for (int j=1; j<=tabDesc[i].getNbDef(); j++) {
				Descripteur courante = tabDesc[i];
				int k;
				//on regarde si la def dans dicoDef contient bien les bonnes informations
				for (k=0; k<nbDef; k++)
					if (dicoDef[k].nomProc.equals(courante.getDefNomProc(j)) && dicoDef[k].nbParam == courante.getDefNbParam(j)) {
						adFinale[i][j] = dicoDef[k].adPo;
						break;
					}
				if (k >= nbDef) erreur(FATALE, "référence introuvable ou informations fausses "+courante.getDefNomProc(j));
			}
		}*/

		if (nbErr > 0) {
			System.out.println("programme ex�cutable non produit");
			System.exit(1);
		}
		
		// Phase 2 de l'edition de liens
		// -----------------------------
		constMap();				// a completer
		System.out.println("Edition de liens terminée");
	}
}
