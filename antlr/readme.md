# Antlr

Put *.g file in the directory and call antlr with the bat like this:
```bash
g2java.bat name.g
```
Antlr will generate a parser and a lexical analyser. You can use them in the code after.

You can exec this task in project module with:
```bash
gradle refreshProject
```
